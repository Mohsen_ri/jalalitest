﻿using System.Threading.Tasks;
using JalaliTest.Dtos;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace JalaliTest.Controllers
{
    public class RentalTimeController : JalaliTestController
    {
        private readonly IRentalTimeService _rentalService;

        public RentalTimeController(IRentalTimeService rentalService)
        {
            _rentalService = rentalService;
        }

        [HttpPost("/api/app/rental-time/RemoveClientId/{id}")]
        public async Task<ActionResult> RemoveClientId(int id)
        {
             await _rentalService.RemoveClientIdsAsync(id);
            return NoContent();
        }

        [HttpPost("/api/app/rental-time/Rent")]
        public async Task<ActionResult> Rent([FromBody] RentVehicleDto rentVehicleDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _rentalService.RentAsync(rentVehicleDto);
            return Ok(result);
        }

        [HttpPost("/api/app/rental-time/CancelRent")]
        public async Task<ActionResult> CancelRent([FromBody] RentVehicleDto rentVehicleDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _rentalService.CancelRentAsync(rentVehicleDto);
            return Ok("Rental Canceled");
        }
    }
}