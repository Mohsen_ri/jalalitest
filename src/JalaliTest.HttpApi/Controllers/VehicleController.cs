﻿using System.Threading.Tasks;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest.Controllers
{
    public class VehicleController : JalaliTestController
    {
        private readonly IVehicleService _vehicleService;
        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }
        [HttpDelete("/api/app/Vehicle/SafeDelete/{id}")]
        public async Task<ActionResult> SoftDelete(int id)
        {
            var result = await _vehicleService.SoftDelete(id);
            if (result)
            {
                return NoContent();
            }

            return NotFound();
        }

    }   
}