﻿using JalaliTest.Localization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace JalaliTest.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class JalaliTestController : AbpController
    {
        protected JalaliTestController()
        {
            LocalizationResource = typeof(JalaliTestResource);
        }
       
    }
}