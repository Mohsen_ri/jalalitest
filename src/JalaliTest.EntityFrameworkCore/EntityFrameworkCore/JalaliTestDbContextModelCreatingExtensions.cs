﻿using JalaliTest.Entities;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace JalaliTest.EntityFrameworkCore
{
    public static class JalaliTestDbContextModelCreatingExtensions
    {
        public static void ConfigureJalaliTest(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            builder.Entity<Client>(c =>
            {
                c.ToTable(JalaliTestConsts.DbTablePrefix + "Clients", JalaliTestConsts.DbSchema);
                c.ConfigureByConvention();
                c.Property(client => client.Name).IsRequired().HasMaxLength(60);
                c.Property(client => client.Lastname).IsRequired().HasMaxLength(60);
            });

            builder.Entity<Vehicle>(v =>
            {
                v.ToTable(JalaliTestConsts.DbTablePrefix + "Vehicles", JalaliTestConsts.DbSchema);
                v.ConfigureByConvention();
                v.Property(vehicle => vehicle.Name).IsRequired();
                v.Property(vehicle => vehicle.IsDelete).IsRequired();
                
            });
            builder.Entity<Vehicle>().HasQueryFilter(p => !p.IsDelete);


            builder.Entity<RentalTime>(r =>
            {
                r.ToTable(JalaliTestConsts.DbTablePrefix + "RentalTimes", JalaliTestConsts.DbSchema);
                r.ConfigureByConvention();
                r.Property(rentalTime => rentalTime.AvailableDate).IsRequired();
                r.Property(rentalTime => rentalTime.IsRented).IsRequired();
                r.Property(rentalTime => rentalTime.Price).IsRequired();
                
                //relations
                r.HasOne<Vehicle>().WithMany().HasForeignKey(x => x.VehicleId).IsRequired();
                r.HasOne<Client>().WithMany().HasForeignKey(x => x.ClientId).IsRequired(false);

            });

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(JalaliTestConsts.DbTablePrefix + "YourEntities", JalaliTestConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}