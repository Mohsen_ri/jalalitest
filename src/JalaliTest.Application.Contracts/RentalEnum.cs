﻿using System.ComponentModel;

namespace JalaliTest
{
    public enum RentalEnum
    {
        [Description("وجود ندارد")]
        NotExits= 0,
        [Description("قیمت ویرایش شد")]
        PriceUpdated = 1,
        [Description("ماشین اجاره شده و نمیشه قیمت رو برای این تاریخ عوض کرد")]
        IsRented = 2,
    }

    public enum RentRequest
    {
        [Description("در این بازه ماشین مورد نظر برای اجاره نیست")]
        NotAvailable = 0,
        [Description("در این بازه ماشین مورد نظر اجاره شد")]
        Ok = 1,
    }
}