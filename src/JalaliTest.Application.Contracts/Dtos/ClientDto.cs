﻿using Volo.Abp.Application.Dtos;

namespace JalaliTest.Dtos
{
    public class ClientDto : AuditedEntityDto<int>
    {
        public string Name { get; set; }
        public string Lastname { get; set; }

    }
}