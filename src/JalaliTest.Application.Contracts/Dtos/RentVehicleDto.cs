﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JalaliTest.Dtos
{
    public class RentVehicleDto
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public int VehicleId { get; set; }
        [Required]
        public int ClientId { get; set; }
    }
}