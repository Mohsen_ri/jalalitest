﻿using System;
using Volo.Abp.Application.Dtos;

namespace JalaliTest.Dtos
{
    public class VehicleDto: AuditedEntityDto<int>
    {
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
    }
}