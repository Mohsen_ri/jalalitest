﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JalaliTest.Dtos
{
    public class RentVehicleResponseDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal? TotalPrice { get; set; }

        public ClientDto Client { get; set; }
        public VehicleDto Vehicle { get; set; }
    }
}