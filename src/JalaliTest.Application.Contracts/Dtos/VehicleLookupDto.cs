﻿using Volo.Abp.Application.Dtos;

namespace JalaliTest.Dtos
{
    public class VehicleLookupDto:EntityDto<int>
    {
        public string Name { get; set; }
    }
}