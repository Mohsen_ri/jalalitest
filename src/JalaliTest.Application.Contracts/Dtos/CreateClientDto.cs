﻿using System.ComponentModel.DataAnnotations;

namespace JalaliTest.Dtos
{
    public class CreateClientDto
    {
        [Required]
        [StringLength(60)]
        public string Name { get; set; }
        [Required]
        [StringLength(60)]
        public string Lastname { get; set; }

    }
}