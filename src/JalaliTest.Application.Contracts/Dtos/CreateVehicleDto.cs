﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JalaliTest.Dtos
{
    public class CreateVehicleDto
    {
        [Required]
        public string Name { get; set; }
    }
}