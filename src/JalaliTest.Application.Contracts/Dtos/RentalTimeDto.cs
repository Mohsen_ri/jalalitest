﻿using System;
using Volo.Abp.Application.Dtos;

namespace JalaliTest.Dtos
{
    public class RentalTimeDto: AuditedEntityDto<int>
    {
        public DateTime AvailableDate { get; set; }
        public bool IsRented { get; set; }
        public decimal Price { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }

        public int? ClientId { get; set; }
    }
}