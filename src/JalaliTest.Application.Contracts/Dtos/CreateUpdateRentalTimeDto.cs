﻿using System;

namespace JalaliTest.Dtos
{
    public class CreateUpdateRentalTimeDto
    {
        public DateTime AvailableDate { get; set; }
        public decimal Price { get; set; }

        public int VehicleId { get; set; }

        public int? ClientId { get; set; }
    }
}