﻿using System.Threading.Tasks;
using JalaliTest.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace JalaliTest.Services.Interfaces
{
    public interface IVehicleService: ICrudAppService< //Defines CRUD methods
        VehicleDto, //Used to show Vehicle
        int, //Primary key of the Vehicle entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateVehicleDto> //Used to create/update a Vehicle
    {
        public Task<bool> SoftDelete(int id);
       
    }
}