﻿using System.Linq.Expressions;
using System.Threading.Tasks;
using JalaliTest.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace JalaliTest.Services.Interfaces
{
    public interface IRentalTimeService: ICrudAppService< //Defines CRUD methods
        RentalTimeDto, //Used to show RentalTime
        int, //Primary key of the RentalTime entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateUpdateRentalTimeDto> //Used to create/update a RentalTime
    {
        public Task RemoveClientIdsAsync(int clientId);

        public Task<RentalEnum> UpdatePriceAsync(CreateUpdateRentalTimeDto createUpdateRentalTimeDto);

        public Task<ListResultDto<VehicleLookupDto>> GetVehicleLookupAsync();

        public Task<RentVehicleResponseDto> RentAsync(RentVehicleDto rentVehicleDto);
        public Task CancelRentAsync(RentVehicleDto rentVehicleDto);
    }
}