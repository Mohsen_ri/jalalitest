﻿using System;
using JalaliTest.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace JalaliTest.Services.Interfaces
{
    public interface IClientService : ICrudAppService< //Defines CRUD methods
        ClientDto, //Used to show Client
        int, //Primary key of the Client entity
        PagedAndSortedResultRequestDto, //Used for paging/sorting
        CreateClientDto> //Used to create/update a Client
    {
        
    }
}