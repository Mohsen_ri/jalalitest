﻿using JalaliTest.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace JalaliTest.Permissions
{
    public class JalaliTestPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(JalaliTestPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(JalaliTestPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<JalaliTestResource>(name);
        }
    }
}
