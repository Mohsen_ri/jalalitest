﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace JalaliTest.Web
{
    [Dependency(ReplaceServices = true)]
    public class JalaliTestBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "JalaliTest";
    }
}
