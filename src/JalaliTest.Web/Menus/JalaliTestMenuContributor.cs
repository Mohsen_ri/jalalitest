﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using JalaliTest.Localization;
using JalaliTest.MultiTenancy;
using Volo.Abp.TenantManagement.Web.Navigation;
using Volo.Abp.UI.Navigation;

namespace JalaliTest.Web.Menus
{
    public class JalaliTestMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            if (!MultiTenancyConsts.IsEnabled)
            {
                var administration = context.Menu.GetAdministration();
                administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
            }

            var l = context.GetLocalizer<JalaliTestResource>();

            context.Menu.Items.Insert(0, new ApplicationMenuItem(JalaliTestMenus.Home, l["Menu:Home"], "~/"));
            context.Menu.Items.Insert(1,new ApplicationMenuItem(JalaliTestMenus.Clients,"Clients","~/clients"));
            context.Menu.Items.Insert(2, new ApplicationMenuItem(JalaliTestMenus.Vehicles, "Vehicles", "~/Vehicles"));
            context.Menu.Items.Insert(3, new ApplicationMenuItem(JalaliTestMenus.SetPriceAndDateForVehicle, "rental", "~/RentalTimes"));

        }
    }
}
