﻿namespace JalaliTest.Web.Menus
{
    public class JalaliTestMenus
    {
        private const string Prefix = "JalaliTest";
        public const string Home = Prefix + ".Home";
        public const string Clients = Prefix + ".Clients";
        public const string Vehicles = Prefix + ".Vehicles";
        public const string SetPriceAndDateForVehicle = Prefix + ".Set Price And Date For Vehicle";
        //Add your menu items here...

    }
}