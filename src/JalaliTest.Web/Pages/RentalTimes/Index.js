﻿$(function () {

    var dataTable = $('#RentalTimesTable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            ajax: abp.libs.datatables.createAjax(jalaliTest.services.implementations.rentalTime.getList),
            columnDefs: [
                {
                    title: 'Actions',
                    rowAction: {
                        items:
                            [
                                {
                                    text: 'Delete',
                                    confirmMessage: function (data) {
                                        return 'Are you sure to remove', data.record.name;
                                    },
                                    action: function (data) {
                                        jalaliTest.services.implementations.rentalTime
                                            .delete(data.record.id)
                                            .then(function () {
                                                abp.notify.info('SuccessfullyDeleted');
                                                dataTable.ajax.reload();
                                            });

                                    }
                                }


                            ]
                    }
                },
                {
                    title: 'VehicleName',
                    data: "vehicleName"
                },
                {
                    title: 'Price',
                    data: "price"
                }, {
                    title: 'AvailableDate',
                    data: "availableDate",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    }
                },
                {
                    title: "CreateDate", data: "creationTime",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    }
                }
            ]
        })
    );
    //AddVehicleButton
    var createVehicle = new abp.ModalManager(abp.appPath + 'RentalTimes/Create');

    //reload table
    createVehicle.onResult(function () {
        dataTable.ajax.reload();
    });

    $('#AddRentalTimeButton').click(function (e) {
        e.preventDefault();
        createVehicle.open();
    });
});