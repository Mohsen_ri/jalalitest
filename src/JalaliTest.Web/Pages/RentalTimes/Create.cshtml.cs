using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using JalaliTest.Dtos;
using JalaliTest.Entities;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest.Web.Pages.RentalTimes
{
    public class CreateModel : JalaliTestPageModel
    {
        [BindProperty]
        public CreateRentalTimeViewModel RentalTimeViewModel { get; set; }

        public List<SelectListItem> Vehicles { get; set; }

        private readonly IRentalTimeService _rentalService;
        public CreateModel(IRentalTimeService rentalService)
        {
            _rentalService = rentalService;

        }

        public async Task OnGetAsync()
        {
            RentalTimeViewModel = new CreateRentalTimeViewModel();
            var vehicleLookup = await _rentalService.GetVehicleLookupAsync();
            Vehicles = vehicleLookup.Items.Select(p => new SelectListItem(p.Name, p.Id.ToString())).ToList();
        }

        public async Task<ActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return NoContent();
            }

            if (RentalTimeViewModel.AvailableDate < DateTime.Now.Date)
            {
                return BadRequest("Not past days");
            }

            var mappedToDto =
                ObjectMapper.Map<CreateRentalTimeViewModel, CreateUpdateRentalTimeDto>(RentalTimeViewModel);
            var result = await _rentalService.UpdatePriceAsync(mappedToDto);
            switch (result)
            {
                case RentalEnum.NotExits:
                    await _rentalService.CreateAsync(mappedToDto);
                    return NoContent();
                case RentalEnum.IsRented:
                    return BadRequest("Rented!!");
                default:
                    return NoContent();
            }
        }
        public class CreateRentalTimeViewModel
        {
            [SelectItems(nameof(Vehicles))]
            [DisplayName("Vehicle")]
            public int VehicleId { get; set; }

            [Required]
            [DataType(DataType.Date)]
            public DateTime AvailableDate { get; set; } = DateTime.Now;

            [Required]
            [Range(1.0, double.MaxValue)]
            public decimal Price { get; set; }
        }
    }
}
