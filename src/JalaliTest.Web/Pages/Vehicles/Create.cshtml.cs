using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JalaliTest.Dtos;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace JalaliTest.Web.Pages.Vehicles
{
    public class CreateModel : JalaliTestPageModel
    {
        private readonly IVehicleService _vehicleService;

        public CreateModel(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        [BindProperty]
        public CreateVehicleDto CreateVehicleDto { get; set; }
        public void OnGet()
        {
            CreateVehicleDto = new CreateVehicleDto();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return NoContent();
            }

            await _vehicleService.CreateAsync(CreateVehicleDto);
            return NoContent();
        }
    }
}
