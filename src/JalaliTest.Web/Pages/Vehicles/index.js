﻿$(function () {

    var dataTable = $('#VehiclesTable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            ajax: abp.libs.datatables.createAjax(jalaliTest.services.implementations.vehicle.getList),
            columnDefs: [
                {
                    title: 'Actions',
                    rowAction: {
                        items:
                            [
                                {
                                    text: 'Delete',
                                    confirmMessage: function (data) {
                                        return 'Are you sure to remove', data.record.name;
                                    },
                                    action: function (data) {
                                        var url = '/api/app/Vehicle/SafeDelete/' + data.record.id;
                                        abp.ajax({
                                            type: 'DELETE',
                                            url: url
                                        }).then(function(data) {
                                            //abp.notify.info('SuccessfullyDeleted');
                                            dataTable.ajax.reload();
                                        });

                                    }
                                }


                            ]
                    }
                },
                {
                    title: 'Name',
                    data: "name"
                },
                {
                    title: "CreateDate", data: "creationTime",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    }
                }
            ]
        })
    );
    //AddVehicleButton
    var createVehicle = new abp.ModalManager(abp.appPath + 'vehicles/Create');

    //reload table
    createVehicle.onResult(function () {
        dataTable.ajax.reload();
    });

    $('#AddVehicleButton').click(function (e) {
        e.preventDefault();
        createVehicle.open();
    });
});