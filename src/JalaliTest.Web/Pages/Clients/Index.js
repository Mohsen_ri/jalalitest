﻿$(function () {
    var l = abp.localization.getResource('JalaliTest');


    var dataTable = $('#ClientsTable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            ajax: abp.libs.datatables.createAjax(jalaliTest.services.implementations.client.getList),
            columnDefs: [
                {
                    title: 'Actions',
                    rowAction: {
                        items:
                            [
                                {
                                    text: 'Delete',
                                    confirmMessage: function (data) {
                                        return 'Are you sure to remove', data.record.name;
                                    },
                                    action: function (data) {
                                        var url = '/api/app/rental-time/RemoveClientId/' + data.record.id;
                                        abp.ajax({
                                            type: 'POST',
                                            url: url,
                                            abpHandleError: false //DISABLE AUTO ERROR HANDLING
                                        }).then(function (result) {
                                            jalaliTest.services.implementations.client
                                                .delete(data.record.id)
                                                .then(function () {
                                                    abp.notify.info('SuccessfullyDeleted');
                                                    dataTable.ajax.reload();
                                                });
                                        })
                                     
                                    }
                                }


                            ]
                    }
                },
                {
                    title: "Name",
                    data: "name"
                },
                {
                    title: "Lastname",
                    data: "lastname"

                },
                {
                    title: l('CreationTime'), data: "creationTime",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    }
                }
            ]
        })
    );
    var createClient = new abp.ModalManager(abp.appPath + 'clients/Create');

    //reload table
    createClient.onResult(function () {
        dataTable.ajax.reload();
    });

    $('#AddClientButton').click(function (e) {
        e.preventDefault();
        createClient.open();
    });

});