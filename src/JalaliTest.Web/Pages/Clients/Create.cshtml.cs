using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JalaliTest.Dtos;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace JalaliTest.Web.Pages.Clients
{
    public class CreateModel : JalaliTestPageModel
    {
        private readonly IClientService _clientService;

        public CreateModel(IClientService clientService)
        {
            _clientService = clientService;
        }
        [BindProperty]
        public CreateClientDto CreateClientDto { get; set; }

        public void OnGet()
        {
            CreateClientDto = new CreateClientDto();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return NoContent();
            }

            await _clientService.CreateAsync(CreateClientDto);
            return NoContent();
        }
    }
}
