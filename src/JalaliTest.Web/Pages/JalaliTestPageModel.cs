﻿using JalaliTest.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace JalaliTest.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class JalaliTestPageModel : AbpPageModel
    {
        protected JalaliTestPageModel()
        {
            LocalizationResourceType = typeof(JalaliTestResource);
        }
    }
}