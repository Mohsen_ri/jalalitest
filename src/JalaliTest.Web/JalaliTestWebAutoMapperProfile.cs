﻿using AutoMapper;
using JalaliTest.Dtos;
using JalaliTest.Web.Pages.RentalTimes;

namespace JalaliTest.Web
{
    public class JalaliTestWebAutoMapperProfile : Profile
    {
        public JalaliTestWebAutoMapperProfile()
        {
            CreateMap<CreateModel.CreateRentalTimeViewModel, CreateUpdateRentalTimeDto>();
            //Define your AutoMapper configuration here for the Web project.
        }
    }
}
