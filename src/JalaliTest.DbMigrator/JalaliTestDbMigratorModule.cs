﻿using JalaliTest.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace JalaliTest.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(JalaliTestEntityFrameworkCoreDbMigrationsModule),
        typeof(JalaliTestApplicationContractsModule)
        )]
    public class JalaliTestDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
