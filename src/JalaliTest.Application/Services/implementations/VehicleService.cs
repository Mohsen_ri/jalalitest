﻿using System.Linq;
using System.Threading.Tasks;
using JalaliTest.Dtos;
using JalaliTest.Entities;
using JalaliTest.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest.Services.implementations
{
    public class VehicleService :
        CrudAppService<
            Vehicle, //The Vehicle entity
            VehicleDto, //Used to show Vehicle
            int, //Primary key of the Vehicle entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateVehicleDto>, //Used to create/update a Client
        IVehicleService //implement the IVehicleService
    {

        private readonly IRepository<Vehicle, int> _vehicleRepository; 
        public VehicleService(IRepository<Vehicle, int> repository)
            : base(repository)
        {
            _vehicleRepository = repository;
        }
        

        [NonAction]
        public async Task<bool> SoftDelete(int id)
        {
            var vehicle = await _vehicleRepository.GetAsync(id);
            if (vehicle is null)
            {
                return false;
            }
            vehicle.IsDelete = true;
            await _vehicleRepository.UpdateAsync(vehicle,true);
            return true;
        }
    
    }
}