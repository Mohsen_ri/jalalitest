﻿using System;
using JalaliTest.Dtos;
using JalaliTest.Entities;
using JalaliTest.Services.Interfaces;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest.Services.implementations
{
    public class ClientService:
        CrudAppService<
            Client, //The Client entity
            ClientDto, //Used to show Client
            int, //Primary key of the Client entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateClientDto>, //Used to create/update a Client
        IClientService //implement the IClientAppService
    {
        public ClientService(IRepository<Client, int> repository)
            : base(repository)
        {

        }
    }
}