﻿using System;
using System.Collections.Generic;
using System.Linq;
using JalaliTest.Dtos;
using JalaliTest.Entities;
using JalaliTest.Services.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest.Services.implementations
{
    public class RentalTimeService : CrudAppService<
            RentalTime, //The RentalTime entity
            RentalTimeDto, //Used to show RentalTime
            int, //Primary key of the RentalTime entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CreateUpdateRentalTimeDto>, //Used to create/update a RentalTime
        IRentalTimeService //implement the IRentalTimeService

    {
        private readonly IRepository<RentalTime, int> _rentalTimesRepository;
        private readonly IRepository<Vehicle, int> _vehiclesRepository;
        private readonly IRepository<Client, int> _clientsRepository;

        public RentalTimeService(IRepository<RentalTime, int> repository, IRepository<Vehicle, int> vehiclesRepository, IRepository<Client, int> clientsRepository)
            : base(repository)
        {
            _rentalTimesRepository = repository;
            _vehiclesRepository = vehiclesRepository;
            _clientsRepository = clientsRepository;
        }

        [NonAction]
        public async Task RemoveClientIdsAsync(int clientId)
        {
            var rentals = await _rentalTimesRepository.GetQueryableAsync();
            var findClientRentals = rentals.Where(q => q.ClientId == clientId).ToList();
            if (findClientRentals.Any())
            {
                foreach (var clientRental in findClientRentals)
                {
                    clientRental.ClientId = null;
                    clientRental.IsRented = false;
                    await _rentalTimesRepository.UpdateAsync(clientRental, true);
                }
            }
        }

        [NonAction]
        public async Task<RentalEnum> UpdatePriceAsync(CreateUpdateRentalTimeDto createUpdateRentalTimeDto)
        {
            var rentalQueryable = await _rentalTimesRepository.GetQueryableAsync();
            var findRental = rentalQueryable.FirstOrDefault(p =>
                p.AvailableDate.Date == createUpdateRentalTimeDto.AvailableDate.Date &&
                p.VehicleId == createUpdateRentalTimeDto.VehicleId);
            if (findRental is null)
            {
                return RentalEnum.NotExits;
            }

            if (findRental.IsRented)
            {
                return RentalEnum.IsRented;
            }

            findRental.Price = createUpdateRentalTimeDto.Price;
            await _rentalTimesRepository.UpdateAsync(findRental, true);
            return RentalEnum.PriceUpdated;

        }

        public override async Task<RentalTimeDto> GetAsync(int id)
        {
            var rentalQueryable = await _rentalTimesRepository.GetQueryableAsync();

            var joinQuery = from rentalTime in rentalQueryable
                            join vehicle in _vehiclesRepository on rentalTime.VehicleId equals vehicle.Id
                            where rentalTime.Id == id
                            select new { rentalTime, vehicle };

            //  var query = from book in queryable
            //      join author in _authorRepository on book.AuthorId equals author.Id
            //      where book.Id == id
            //      select new { book, author };

            var queryResult = await AsyncExecuter.FirstOrDefaultAsync(joinQuery);
            if (queryResult is null)
            {
                throw new EntityNotFoundException(typeof(RentalTime), id);
            }

            var rentalDto = ObjectMapper.Map<RentalTime, RentalTimeDto>(queryResult.rentalTime);
            rentalDto.VehicleName = queryResult.vehicle.Name;
            return rentalDto;
        }

        public override async Task<PagedResultDto<RentalTimeDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            //sort by id
            if (input.Sorting.IsNullOrWhiteSpace())
            {
                input.Sorting = nameof(RentalTime.Id);
            }

            var rentalQueryable = await _rentalTimesRepository.GetQueryableAsync();

            var query = from rentalTime in rentalQueryable
                        join vehicle in _vehiclesRepository on rentalTime.VehicleId equals vehicle.Id
                        select new { rentalTime, vehicle };

            //paging
            query = query.Skip(input.SkipCount).Take(input.MaxResultCount);

            var queryResult = await AsyncExecuter.ToListAsync(query);


            var rentalTimeDtos = queryResult.Select(s =>
            {
                var rentalTimeDto = ObjectMapper.Map<RentalTime, RentalTimeDto>(s.rentalTime);
                rentalTimeDto.VehicleName = s.vehicle.Name;
                return rentalTimeDto;
            }).ToList();


            var totalCount = await _rentalTimesRepository.GetCountAsync();

            return new PagedResultDto<RentalTimeDto>(totalCount, rentalTimeDtos);
        }

        [NonAction]
        public async Task<ListResultDto<VehicleLookupDto>> GetVehicleLookupAsync()
        {
            var vehicles = await _vehiclesRepository.GetListAsync();

            return new ListResultDto<VehicleLookupDto>(
                ObjectMapper.Map<List<Vehicle>, List<VehicleLookupDto>>(vehicles));
        }

        [NonAction]
        public async Task<RentVehicleResponseDto> RentAsync(RentVehicleDto rentVehicleDto)
        {
            var rentalQueryable = await _rentalTimesRepository.GetQueryableAsync();

            #region check date range
            //این متغیر برای بدست آوردن تعداد روز های بین دو تاریخ هست که برای جلوگیری از بر نگرداندن 
            //query
            //اشتباه استفاده شده
            var totalDays = (rentVehicleDto.EndDate - rentVehicleDto.StartDate).TotalDays + 1;
            var vehicleFreeDaysCount = await rentalQueryable.Where(p => p.AvailableDate.Date >= rentVehicleDto.StartDate.Date &&
                    p.AvailableDate.Date <= rentVehicleDto.EndDate.Date && p.VehicleId == rentVehicleDto.VehicleId && p.IsRented == false).CountAsync();

            if (Math.Abs((double)vehicleFreeDaysCount - totalDays) > 0.0000001)
            {
                throw new UserFriendlyException("bad date range");
            }
            #endregion

            var rentalList = await rentalQueryable.Where(p => p.AvailableDate.Date >= rentVehicleDto.StartDate.Date &&
                                                              p.AvailableDate.Date <= rentVehicleDto.EndDate.Date &&
                                                              p.VehicleId == rentVehicleDto.VehicleId &&
                                                              p.IsRented == false).ToListAsync();

            foreach (var rent in rentalList)
            {
                rent.IsRented = true;
                rent.ClientId = rentVehicleDto.ClientId;
            }
            await _rentalTimesRepository.UpdateManyAsync(rentalList, true);

            #region Preparing resut and mapping Dtos
            var client = await _clientsRepository.GetAsync(rentVehicleDto.ClientId);
            var vehicle = await _vehiclesRepository.GetAsync(rentVehicleDto.VehicleId);

            var rentVehicleResponseDto = ObjectMapper.Map<RentVehicleDto, RentVehicleResponseDto>(rentVehicleDto);
            rentVehicleResponseDto.Client = ObjectMapper.Map<Client, ClientDto>(client);
            rentVehicleResponseDto.Vehicle = ObjectMapper.Map<Vehicle, VehicleDto>(vehicle);
            rentVehicleResponseDto.TotalPrice = rentalList.Sum(s => s.Price);

            #endregion
            return rentVehicleResponseDto;
        }
        [NonAction]
        public async Task CancelRentAsync(RentVehicleDto rentVehicleDto)
        {
            var rentalQueryable = await _rentalTimesRepository.GetQueryableAsync();

            #region check date range
            //این متغیر برای بدست آوردن تعداد روز های بین دو تاریخ هست که برای جلوگیری از بر نگرداندن 
            //query
            //اشتباه استفاده شده
            var totalDays = (rentVehicleDto.EndDate - rentVehicleDto.StartDate).TotalDays + 1;
            var vehicleFreeDaysCount = await rentalQueryable.Where(p => p.AvailableDate.Date >= rentVehicleDto.StartDate.Date &&
                                                                        p.AvailableDate.Date <= rentVehicleDto.EndDate.Date && p.VehicleId == rentVehicleDto.VehicleId && p.IsRented == true).CountAsync();

            if (Math.Abs((double)vehicleFreeDaysCount - totalDays) > 0.0000001)
            {
                throw new UserFriendlyException("bad date range");
            }
            #endregion

            var rentalList = await rentalQueryable.Where(p => p.AvailableDate.Date >= rentVehicleDto.StartDate.Date &&
                                                              p.AvailableDate.Date <= rentVehicleDto.EndDate.Date &&
                                                              p.VehicleId == rentVehicleDto.VehicleId &&
                                                              p.IsRented).ToListAsync();

            foreach (var rent in rentalList)
            {
                rent.IsRented = false;
                rent.ClientId = null;
            }

            await _rentalTimesRepository.UpdateManyAsync(rentalList, true);
        }

    }

}