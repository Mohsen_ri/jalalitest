﻿using System;
using System.Collections.Generic;
using System.Text;
using JalaliTest.Localization;
using Volo.Abp.Application.Services;

namespace JalaliTest
{
    /* Inherit your application services from this class.
     */
    public abstract class JalaliTestAppService : ApplicationService
    {
        protected JalaliTestAppService()
        {
            LocalizationResource = typeof(JalaliTestResource);
        }
    }
}
