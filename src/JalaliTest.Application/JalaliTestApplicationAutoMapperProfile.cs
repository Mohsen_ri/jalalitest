﻿using AutoMapper;
using JalaliTest.Dtos;
using JalaliTest.Entities;

namespace JalaliTest
{
    public class JalaliTestApplicationAutoMapperProfile : Profile
    {
        public JalaliTestApplicationAutoMapperProfile()
        {
            CreateMap<Client, ClientDto>();
            CreateMap<CreateClientDto, Client>();

            CreateMap<Vehicle, VehicleDto>();
            CreateMap<CreateVehicleDto, Vehicle>();
            CreateMap<Vehicle, VehicleLookupDto>();


            CreateMap<RentalTime, RentalTimeDto>();
            CreateMap<CreateUpdateRentalTimeDto, RentalTime>().ReverseMap();
            CreateMap<RentVehicleDto, RentVehicleResponseDto>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
