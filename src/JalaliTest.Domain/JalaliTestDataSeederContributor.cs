﻿using System;
using System.Threading.Tasks;
using JalaliTest.Entities;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace JalaliTest
{
    public class JalaliTestDataSeederContributor : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Client, int> _clientRepository;
        private readonly IRepository<Vehicle, int> _vehicleRepository;
        private readonly IRepository<RentalTime, int> _rentalTimeRepository;

        public JalaliTestDataSeederContributor(IRepository<Client, int> clientRepository, IRepository<Vehicle, int> vehicleRepository, IRepository<RentalTime, int> rentalTimeRepository)
        {
            _clientRepository = clientRepository;
            _vehicleRepository = vehicleRepository;
            _rentalTimeRepository = rentalTimeRepository;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (await _clientRepository.GetCountAsync()>0 )
            {
                return;
            }
            #region Seed Clients

            var mohsenClient = await _clientRepository.InsertAsync(new Client
            {
                Name = "Mohsen",
                Lastname = "Rafiei"
            }, autoSave: true);

            var testClient = await _clientRepository.InsertAsync(new Client
            {
                Name = "test",
                Lastname = "testi"
            }, autoSave: true);
            #endregion


            #region Seed Vehicles

            var motorVehicle = await _vehicleRepository.InsertAsync(new Vehicle
            {
                Name= "motor",
                IsDelete = false
            }, autoSave: true);

            var bikeVehicle = await _vehicleRepository.InsertAsync(new Vehicle
            {
                Name = "bike",
                IsDelete = false
            }, autoSave: true);

            var carVehicle = await _vehicleRepository.InsertAsync(new Vehicle
            {
                Name = "car",
                IsDelete = false
            }, autoSave: true);


            var testVehicle = await _vehicleRepository.InsertAsync(new Vehicle
            {
                Name = "test",
                IsDelete = true
            }, autoSave: true);


            var dasdasdasVehicle = await _vehicleRepository.InsertAsync(new Vehicle
            {
                Name = "dasdasdas",
                IsDelete = true
            }, autoSave: true);

            #endregion


            #region Seed Rentals
            //1
            await _rentalTimeRepository.InsertAsync(new RentalTime
            {
                AvailableDate = new DateTime(2021, 02, 08),
                IsRented = false,
                Price = 6.00m,
                VehicleId = motorVehicle.Id,
                ClientId = null
            }, autoSave: true);

            //2
            await _rentalTimeRepository.InsertAsync(new RentalTime
            {
                AvailableDate = new DateTime(2021, 02, 10),
                IsRented = false,
                Price = 10.00m,
                VehicleId = motorVehicle.Id,
                ClientId = null
            }, autoSave: true);

            //3
            await _rentalTimeRepository.InsertAsync(new RentalTime
            {
                AvailableDate = new DateTime(2021, 02, 08),
                IsRented = true,
                Price = 2.00m,
                VehicleId = bikeVehicle.Id,
                ClientId = mohsenClient.Id
            }, autoSave: true);
            //4
            await _rentalTimeRepository.InsertAsync(new RentalTime
            {
                AvailableDate = new DateTime(2021, 02, 09),
                IsRented = true,
                Price = 20.00m,
                VehicleId = bikeVehicle.Id,
                ClientId = mohsenClient.Id
            }, autoSave: true);
            //5
            await _rentalTimeRepository.InsertAsync(new RentalTime
            {
                AvailableDate = new DateTime(2021, 02, 10),
                IsRented = true,
                Price = 20.00m,
                VehicleId = bikeVehicle.Id,
                ClientId = mohsenClient.Id
            }, autoSave: true);

      
            #endregion
        }
    }
}