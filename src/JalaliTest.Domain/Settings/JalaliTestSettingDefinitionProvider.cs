﻿using Volo.Abp.Settings;

namespace JalaliTest.Settings
{
    public class JalaliTestSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(JalaliTestSettings.MySetting1));
        }
    }
}
