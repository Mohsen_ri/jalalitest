﻿using System.Security.AccessControl;
using Volo.Abp.Domain.Entities.Auditing;

namespace JalaliTest.Entities
{
    public class Client: AuditedAggregateRoot<int>    
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}