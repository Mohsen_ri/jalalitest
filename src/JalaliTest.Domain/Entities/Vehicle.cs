﻿using System;
using System.Security.AccessControl;
using Volo.Abp.Domain.Entities.Auditing;

namespace JalaliTest.Entities
{
    public class Vehicle: AuditedAggregateRoot<int>
    {
        public string Name { get; set; }
        public bool IsDelete { get; set; }
    }
}