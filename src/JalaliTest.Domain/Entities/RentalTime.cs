﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace JalaliTest.Entities
{
    public class RentalTime : AuditedAggregateRoot<int>
    {
        public DateTime AvailableDate { get; set; }
        public bool IsRented { get; set; }
        public decimal Price { get; set; }
        public int VehicleId { get; set; }
        public int? ClientId { get; set; }
    }
}