﻿using System.Threading.Tasks;

namespace JalaliTest.Data
{
    public interface IJalaliTestDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
