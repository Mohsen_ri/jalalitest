﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace JalaliTest.Data
{
    /* This is used if database provider does't define
     * IJalaliTestDbSchemaMigrator implementation.
     */
    public class NullJalaliTestDbSchemaMigrator : IJalaliTestDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}