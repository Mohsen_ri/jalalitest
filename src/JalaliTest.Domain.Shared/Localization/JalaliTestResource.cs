﻿using Volo.Abp.Localization;

namespace JalaliTest.Localization
{
    [LocalizationResourceName("JalaliTest")]
    public class JalaliTestResource
    {

    }
}