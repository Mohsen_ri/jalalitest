﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JalaliTest.Migrations
{
    public partial class Remove_Soft_Delete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "AppClients");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "AppClients",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
