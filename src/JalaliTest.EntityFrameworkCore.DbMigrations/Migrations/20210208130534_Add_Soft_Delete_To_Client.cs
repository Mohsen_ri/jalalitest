﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JalaliTest.Migrations
{
    public partial class Add_Soft_Delete_To_Client : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "AppClients",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "AppClients");
        }
    }
}
