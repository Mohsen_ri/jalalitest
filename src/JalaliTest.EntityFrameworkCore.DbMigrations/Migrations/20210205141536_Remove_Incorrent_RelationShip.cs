﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JalaliTest.Migrations
{
    public partial class Remove_Incorrent_RelationShip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppVehicles_AppClients_ClientId",
                table: "AppVehicles");

            migrationBuilder.DropIndex(
                name: "IX_AppVehicles_ClientId",
                table: "AppVehicles");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "AppRentalTimes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppRentalTimes_ClientId",
                table: "AppRentalTimes",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppRentalTimes_AppClients_ClientId",
                table: "AppRentalTimes",
                column: "ClientId",
                principalTable: "AppClients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppRentalTimes_AppClients_ClientId",
                table: "AppRentalTimes");

            migrationBuilder.DropIndex(
                name: "IX_AppRentalTimes_ClientId",
                table: "AppRentalTimes");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "AppRentalTimes");

            migrationBuilder.CreateIndex(
                name: "IX_AppVehicles_ClientId",
                table: "AppVehicles",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppVehicles_AppClients_ClientId",
                table: "AppVehicles",
                column: "ClientId",
                principalTable: "AppClients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
