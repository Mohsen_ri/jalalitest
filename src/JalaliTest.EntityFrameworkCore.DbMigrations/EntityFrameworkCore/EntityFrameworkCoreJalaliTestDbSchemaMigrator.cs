﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using JalaliTest.Data;
using Volo.Abp.DependencyInjection;

namespace JalaliTest.EntityFrameworkCore
{
    public class EntityFrameworkCoreJalaliTestDbSchemaMigrator
        : IJalaliTestDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreJalaliTestDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the JalaliTestMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<JalaliTestMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}