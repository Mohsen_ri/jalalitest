﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace JalaliTest.EntityFrameworkCore
{
    /* This class is needed for EF Core console commands
     * (like Add-Migration and Update-Database commands) */
    public class JalaliTestMigrationsDbContextFactory : IDesignTimeDbContextFactory<JalaliTestMigrationsDbContext>
    {
        public JalaliTestMigrationsDbContext CreateDbContext(string[] args)
        {
            JalaliTestEfCoreEntityExtensionMappings.Configure();

            var configuration = BuildConfiguration();

            var builder = new DbContextOptionsBuilder<JalaliTestMigrationsDbContext>()
                .UseSqlServer(configuration.GetConnectionString("Default"));

            return new JalaliTestMigrationsDbContext(builder.Options);
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../JalaliTest.DbMigrator/"))
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
