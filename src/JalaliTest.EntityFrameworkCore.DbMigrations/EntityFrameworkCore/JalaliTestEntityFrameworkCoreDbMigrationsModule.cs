﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace JalaliTest.EntityFrameworkCore
{
    [DependsOn(
        typeof(JalaliTestEntityFrameworkCoreModule)
        )]
    public class JalaliTestEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<JalaliTestMigrationsDbContext>();
        }
    }
}
