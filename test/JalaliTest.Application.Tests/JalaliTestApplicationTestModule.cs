﻿using Volo.Abp.Modularity;

namespace JalaliTest
{
    [DependsOn(
        typeof(JalaliTestApplicationModule),
        typeof(JalaliTestDomainTestModule)
        )]
    public class JalaliTestApplicationTestModule : AbpModule
    {

    }
}