﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace JalaliTest.Pages
{
    public class Index_Tests : JalaliTestWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
