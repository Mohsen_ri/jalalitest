﻿using JalaliTest.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace JalaliTest
{
    [DependsOn(
        typeof(JalaliTestEntityFrameworkCoreTestModule)
        )]
    public class JalaliTestDomainTestModule : AbpModule
    {

    }
}